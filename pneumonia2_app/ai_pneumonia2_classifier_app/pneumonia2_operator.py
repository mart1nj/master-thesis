from typing import Dict, Text
import json
import torch
import numpy as np

from monai.data import DataLoader, Dataset
import monai.deploy.core as md  # 'md' stands for MONAI Deploy (or can use 'core' instead)
from monai.deploy.operators.monai_seg_inference_operator import InMemImageReader
from monai.deploy.core import (
    ExecutionContext,
    Image,
    InputContext,
    IOType,
    Operator,
    OutputContext,
)
from monai.transforms import (
    AddChannel,
    Compose, 
    EnsureChannelFirst, 
    EnsureType, 
    Lambda,
    LoadImage, 
    Resize, 
    ScaleIntensity, 
    SqueezeDim
)

MEDNIST_CLASSES = ["None", "Pneumonia"]

@md.input("image", Image, IOType.IN_MEMORY)
@md.output("result_text", Text, IOType.IN_MEMORY)
@md.env(pip_packages=["monai"])
class MedNISTClassifierOperator(Operator):
    """Classifies the given image and returns the class name."""

    def __init__(self):
        super().__init__()
        self._input_dataset_key = "image"
        self._pred_dataset_key = "pred"

    def compute(self, op_input: InputContext, op_output: OutputContext, context: ExecutionContext):
        input_image = op_input.get("image")
        _reader = InMemImageReader(input_image)
        input_img_metadata = self._convert_dicom_metadata_datatype(input_image.metadata())
        img_name = str(input_img_metadata.get("SeriesInstanceUID", "Img_in_context"))


        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        model = context.models.get()  # get a TorchScriptModel object

        pre_transforms = self.pre_process(_reader)  # (1, 64, 64), torch.float64
        post_transforms = self.post_process()

        dataset = Dataset(data=[{self._input_dataset_key: img_name}], transform=pre_transforms)
        dataloader = DataLoader(dataset, batch_size=10, shuffle=False, num_workers=0)

        with torch.no_grad():
            for d in dataloader:
                image = d.to(device)
                outputs = model(image)
                out = post_transforms(outputs).data.cpu().numpy()[0]
        
        _, output_classes = outputs.max(dim=1)

        result = {}
        counter = 0
        result["Predicted result"] = MEDNIST_CLASSES[output_classes[0]]  # get the class name
        for prediction in out:
            result[MEDNIST_CLASSES[counter]] = prediction.astype('str')
            counter = counter + 1
        print(result)

        # Get output (folder) path and create the folder if not exists
        output_folder = context.output.get().path
        output_folder.mkdir(parents=True, exist_ok=True)

        # Write result to "output.json"
        output_path = output_folder / "output.json"
        with open(output_path, "w") as fp:
            json.dump(result, fp, indent=2)

        result_string = '\n'.join([f'{key}:: {value}' for key, value in result.items()])
        print(result_string)
        op_output.set(result_string, "result_text")


    def _convert_dicom_metadata_datatype(self, metadata: Dict):
        if not metadata:
            return metadata

        # Try to convert data type for the well knowned attributes. Add more as needed.
        if metadata.get("SeriesInstanceUID", None):
            try:
                metadata["SeriesInstanceUID"] = str(metadata["SeriesInstanceUID"])
            except Exception:
                pass
        if metadata.get("row_pixel_spacing", None):
            try:
                metadata["row_pixel_spacing"] = float(metadata["row_pixel_spacing"])
            except Exception:
                pass
        if metadata.get("col_pixel_spacing", None):
            try:
                metadata["col_pixel_spacing"] = float(metadata["col_pixel_spacing"])
            except Exception:
                pass

        print("Converted Image object metadata:")
        for k, v in metadata.items():
            print(f"{k}: {v}, type {type(v)}")

        return metadata

    
    def pre_process(self, image_reader) -> Compose:
        return Compose(
            [
                LoadImage(reader=image_reader, image_only=True),
                EnsureChannelFirst(),
                SqueezeDim(dim=3),
                Lambda(func=lambda x: np.mean(x, axis=0) if len(x.shape) == 3 else x),
                AddChannel(),
                ScaleIntensity(),
                Resize([256, 256]),
                EnsureType()
            ]
        )

    def post_process(self) -> Compose:
        return Compose([EnsureType()])
