import monai.deploy.core as md  # 'md' stands for MONAI Deploy (or can use 'core' instead)
from monai.deploy.core import Application
from monai.deploy.operators.dicom_data_loader_operator import DICOMDataLoaderOperator
from monai.deploy.operators.dicom_series_selector_operator import DICOMSeriesSelectorOperator
from monai.deploy.operators.dicom_series_to_volume_operator import DICOMSeriesToVolumeOperator
from monai.deploy.operators.dicom_text_sr_writer_operator import DICOMTextSRWriterOperator, EquipmentInfo, ModelInfo

from pneumonia_operator import MedNISTClassifierOperator

Sample_Rules_Text = """
{
    "selections": [
        {
            "name": "CR Series",
            "conditions": {
                "Modality": "(?i)CR",
                "PhotometricInterpretation": "MONOCHROME2"
            }
        }
    ]
}
"""

@md.resource(cpu=1, gpu=1, memory="1Gi")
@md.env(pip_packages=["highdicom>=0.18.2", "scipy>=1.9.3", "torch==1.13.0"])
class App(Application):
    """Application class for the MedNIST classifier."""

    def compose(self):
        model_info = ModelInfo(
            name="Pneumonia Classification Model",
            version="0.1",
        )

        classifier_op = MedNISTClassifierOperator()
        my_equipment = EquipmentInfo(manufacturer="MONAI Deploy App SDK", manufacturer_model="DICOM SR Writer")
        my_special_tags = {"SeriesDescription": "Pneumonia evaluation"}
        study_loader_op = DICOMDataLoaderOperator()
        series_selector_op = DICOMSeriesSelectorOperator(rules=Sample_Rules_Text)
        series_to_vol_op = DICOMSeriesToVolumeOperator()


        sr_writer_op = DICOMTextSRWriterOperator(
            copy_tags=True, model_info=model_info, equipment_info=my_equipment,custom_tags=my_special_tags
        )  # copy_tags=True to use Study and Patient modules of the original input

        self.add_flow(study_loader_op, series_selector_op, {"dicom_study_list": "dicom_study_list"})
        self.add_flow(
            series_selector_op, series_to_vol_op, {"study_selected_series_list": "study_selected_series_list"}
        )
        self.add_flow(series_to_vol_op, classifier_op, {"image": "image"})
        self.add_flow(classifier_op, sr_writer_op, {"result_text": "classification_result"})
        # Pass the Study series to the SR writer for copying tags
        self.add_flow(series_selector_op, sr_writer_op, {"study_selected_series_list": "study_selected_series_list"})


if __name__ == "__main__":
    app = App(do_run=True)
