import os
import logging

import requests
from requests.auth import HTTPBasicAuth
import sys

directory = "/app/dicom_converter/data"
dicomServerUrl = os.getenv('DICOM_SERVER')
username = os.getenv('DICOM_SERVER_USERNAME')
password = os.getenv('DICOM_SERVER_PASSWORD')

offsetFile = directory + "/offset.txt"
logfile = directory + '/log.txt'

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s', filename=logfile)

def dicomServerGetRequest(request, headers = [], payload = []):
    url = f'{dicomServerUrl}{request}'
    try:
        if username and password:
            return requests.get(url, headers=headers, data=payload,
                auth = HTTPBasicAuth(username, password)
            )
        else:
            return requests.get(url, headers=headers, data=payload)
    except ConnectionError:
        logging.info("failed getting data from DICOM server")
        sys.exit()

def importDicomFile(dicomSaveDirectory, studyInstanceUid, seriesInstanceUid, sopInstanceUid):
    request = f'studies/{studyInstanceUid}/series/{seriesInstanceUid}/instances/{sopInstanceUid}'
    headers = {
        'Accept': 'application/dicom; transfer-syntax=*'
    }

    response = dicomServerGetRequest(request, headers)

    if response.status_code != requests.codes.ok:
        logging.info("Something went wrong, code " + str(response.status_code))
        sys.exit()

    directory = f'{dicomSaveDirectory}{studyInstanceUid}/{seriesInstanceUid}'
    os.makedirs(directory, exist_ok=True)

    with open(f'{directory}/{sopInstanceUid}.dcm', 'wb+') as f:
        f.write(response.content)

def getOffset():
    with open(offsetFile, 'r') as f:
        return int(f.readline())

def setOffset(offset):
    with open(offsetFile, 'w') as f:
        f.write(offset)    

def importDicomServerChanges(dicomSaveDirectory):
    offset = getOffset()
    request = "changefeed?offset=" + str(offset)
    response = dicomServerGetRequest(request)

    data = []
    if response.status_code == requests.codes.ok:
        data = response.json()
    else:
        logging.info("Something went wrong, code " + str(response.status_code))
        sys.exit()

    if len(data) == 0:
      #  logging.info("No new DICOMs found")
        sys.exit()

    for n in data:
        offset += 1
        if n["action"] == "Create" and n["state"] != 'Deleted':
            studyInstanceUid = n["studyInstanceUid"]
            seriesInstanceUid = n["seriesInstanceUid"]
            sopInstanceUid = n["sopInstanceUid"]
            print("imported " + sopInstanceUid)
            importDicomFile(dicomSaveDirectory, studyInstanceUid, seriesInstanceUid, sopInstanceUid)
    setOffset(str(offset))

