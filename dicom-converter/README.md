This is a modified version of the dicom converter found in CVAT (https://github.com/opencv/cvat/tree/develop/utils/dicom_converter). With added support for getting images from Microsoft DICOM Server + enabling option to convert images to NIfTI. Additional support for 12-bit images. 


# Description

The script is used to convert some kinds of DICOM data to regular images.
Then you can annotate these images on CVAT and get a segmentation mask.
The conversion script was tested on CT, MT and some multi-frame DICOM data.
DICOM files with series (multi-frame) are saved under the same name with a number postfix: 001, 002, 003, etc.

# Installation

```bash
python3 -m venv .env
. .env/bin/activate
pip install -r requirements.txt
```

# Running

```
. .env/bin/activate # if not activated
python script.py input_data output_data
```


# Custom info for version modified with this thesis
Make sure you have folder named data and folder in it named: datasets, imported, nifti + files log.txt, offset.txt

* Set up parameters in .env file: DICOM_SERVER, DICOM_SERVER_USERNAME, DICOM_SERVER_PASSWORD

* **Set up container**
docker-compose -f dicom_converter/docker-compose.yml up -d
Container sends changefeed request to dicom server every 1 minute; you can change interval in crontab file

* Change volume path in docker-compose according to need

* **Access logs**
docker logs dicom-converter_1

* Toggle value convertToNifti in script.py to **enable/disable converting to NIfTI**. 
