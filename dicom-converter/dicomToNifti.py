import os

import dicom2nifti
import dicom2nifti.settings as settings

def convertImportedFilesToNifti(dicomDirectory, niftiDirectory):
    settings.disable_validate_slicecount()
    for dicom in os.listdir(dicomDirectory):
        # TODO maybe dicoms to separate directories?
      #  dicom2nifti.convert_directory(dicomDirectory + dicom, niftiDirectory + dicom)
        dicom2nifti.convert_directory(dicomDirectory + dicom, niftiDirectory)